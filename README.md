## Requirements
1. PHP >= 8.0
1. Node JS >= 13.0
1. NPM >= 6.0
1. Postgre SQL 12

## Setting ENV
1. Pastikan ada 1 konfigurasi koneksi

   Koneksi DB (Postgre SQL)
   ``````
   DB_CONNECTION=pgsql
   DB_HOST=127.0.0.1
   DB_PORT=5432 (port default)
   DB_DATABASE=laravolt (sesuaikan dengan nama db Postgres SQL)
   DB_USERNAME=root (sesuaikan dengan username db Postgres SQL)
   DB_PASSWORD= (sesuaikan dengan password db Postgres SQL)
   ``````
   
## Setup
1. Cloning repository.
1. Pastikan folder berikut dapat ditulis oleh server web:
	1. `storage`
	1. `bootstrap/cache`
1. Run command `composer install`.
1. Run command `npm install`.
1. Run command `npm run dev` untuk dev atau `npm run prod` untuk production.
1. Copy `.env.example` menjadi `.env` dan sesuaikan isinya.
1. Run command `php artisan key:generate`.
1. Run command `php artisan migrate:fresh --seed`.
1. Run command `php artisan storage:link`.
1. Run command `php artisan laravolt:link`.
1. Run command `php artisan serve` atau gunakan valet lalu buka URL yang disediakan oleh konsol di browser.

